x="ERR480581"
bwa index genomes.fna
bwa mem -P -a -S genomes.fna ${x}.fasta> ${x}.sam
samtools view -bS ${x}.sam -o ${x}.bam
samtools sort ${x}.bam ${x}-sorted
samtools index ${x}-sorted.bam 
python bam_process.py ${x}-sorted.bam ${x}-processed
CoreProbe ${x}-processed ${x}-result
